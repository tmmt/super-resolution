"""
Prediction module.
"""

import tensorflow as tf

from libs import Dataset, Folder
from libs.networks import Generator


FLAGS = tf.app.flags.FLAGS


tf.app.flags.DEFINE_integer(
    'batch_size',
    64,
    "Number of samples per batch."
)

tf.app.flags.DEFINE_string(
    'checkpoint',
    './checkpoint/generator/model',
    "Checkpoint file location"
)

tf.app.flags.DEFINE_string(
    'dataset',
    './dataset/*',
    "Path to the dataset directory."
)


def main(argv=None):
    """
    This function is called from its tf.app.run() wrapper.
    """

    generator = Generator()

    generator.predict(
        Dataset(FLAGS.dataset),
        Folder.prediction(),
        FLAGS.checkpoint,
        compare=True
    )


if __name__ == '__main__':
    tf.app.run()
