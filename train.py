"""
Training module.

Todo:
    * Use tensorboard logs.
    * Add support for dynamic learning_rate.
    * Add continue option to continue training from a checkpoint.
"""

import tensorflow as tf

from libs import Dataset, Folder
from libs.networks import DCGAN


FLAGS = tf.app.flags.FLAGS


# configuration
tf.app.flags.DEFINE_float(
    'adam_beta1',
    0.5,
    "Adam optimizer beta1 argument"
)

tf.app.flags.DEFINE_integer(
    'batch_size',
    64,
    "Number of samples per batch."
)

tf.app.flags.DEFINE_integer(
    'checkpoint_period',
    100,
    "Number of iterations (batch steps) in between checkpoints"
)

tf.app.flags.DEFINE_string(
    'dataset',
    './dataset',
    "Path to the dataset directory."
)

tf.app.flags.DEFINE_float(
    'generator_loss_factor',
    0.9,
    "Multiplier for image difference in generator loss function"
)

tf.app.flags.DEFINE_integer(
    'epochs',
    10,
    "Number of epochs to train the model"
)

tf.app.flags.DEFINE_float(
    'learning_rate',
    1e-4,
    "Initial learning rate"
)

tf.app.flags.DEFINE_boolean(
    'original_loss',
    True,
    "Use an original image for a difference in Generator loss function"
)

tf.app.flags.DEFINE_string(
    'validation',
    './validation',
    "Path to the validation dataset directory."
)


def main(argv=None):
    """
    This function is called from its tf.app.run() wrapper.
    """

    dcgan = DCGAN()
    dcgan.train(
        Dataset(FLAGS.dataset),
        Dataset(FLAGS.validation),
        Folder.training(),
        epochs=FLAGS.epochs
    )


if __name__ == '__main__':
    tf.app.run()
