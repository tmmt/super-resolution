# Image super-resolution

A project created for Machine Learning at Faculty of Science, University of
Zagreb. Problem tackled in this project is image super-resolution.
Implemented solution uses state-of-the-art DCGAN network model.

Along with source code, you can check out [presentation.pdf (croatian)](https://tmmt.gitlab.io/pdfs/super-resolution/presentation.pdf "super-resolution presentation")
 and [paper.pdf (croatian)](https://tmmt.gitlab.io/pdfs/super-resolution/paper.pdf "super-resolution paper").

### dependencies
`python3` and all Python dependencies listed in `requirements.txt`.

***
### train
```sh
python3 train.py --adam_beta1 0.5 \
                 --batch_size 64 \
                 --checkpoint_period 100 \
                 --dataset "path/to/training/dataset/*" \
                 --generator_loss_factor 0.8 \
                 --epochs 10 \
                 --learning_rate 0.0001 \
                 --original_loss \
                 --validation 'path/to/validation/dataset/*'
```

***
### predict
```sh
python3 train.py --batch_size 64 \
                 --checkpoint 'path/to/generator/checkpoint' \
                 --dataset "path/to/test/dataset/*"
```
***
