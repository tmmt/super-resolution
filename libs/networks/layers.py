"""
Neural network layers implementations.
"""

import tensorflow as tf


def conv2d_block(network, num_outputs, repeat=2, scope="conv2d_block", activation=None):
    """
    Create a convolutional block. Use striding instead of pooling.

    Args:
        network (Model): A network Model instance to use.
        num_outputs (int): Number of filters per convolutional layer.
        repeat (int): Number of convolutional layers in convolutional block.
        scope (str): Variable scope name.

    Returns:
        Model: Updated network Model.
    """

    assert repeat > 0, "Number of convolutional layers must be at least 1"

    with tf.variable_scope(scope):
        layer = network.output
        for _ in range(repeat - 1):
            layer = tf.layers.conv2d(layer, num_outputs, 5, 1, 'same')

        layer = tf.layers.conv2d(
            layer, num_outputs, 5, 2, 'same', activation=activation
        )

    network.outputs.append(layer)
    return network


def conv2d_transpose_block(network, num_outputs, repeat=2,
                           scope="conv2d_transpose_block",
                           activation=None):
    """
    Create a deconvolutional (transposed convolutional) block. Use striding
    instead of upscaling.

    Args:
        network (Model): A network Model instance to use.
        num_outputs (int): Number of filters per deconvolutional layer.
        repeat (int): Number of deconvolutional layers in deconvolutional block.
        scope (str): Variable scope name.

    Returns:
        Model: Updated network Model.
    """

    assert repeat > 0, "Number of deconvolutional layers must be at least 1"

    with tf.variable_scope(scope):
        layer = network.output
        for _ in range(repeat - 1):
            layer = tf.layers.conv2d_transpose(layer, num_outputs, 5, 1, 'same')

        layer = tf.layers.conv2d_transpose(
            layer, num_outputs, 5, 2, 'same', activation=activation
        )

    network.outputs.append(layer)
    return network


def dropout(network, rate=0.5, training=True, scope="dropout"):
    """
    Create a dropout layer.

    Args:
        network (Model): A network Model instance to use.
        rate (float): The dropout rate, between 0 and 1. E.g. "rate=0.1" would
            drop out 10% of input units.
        training: Either a Python boolean, or a TensorFlow boolean scalar
            tensor (e.g. a placeholder). Whether to return the output in
            training mode (apply dropout) or in inference mode (return the
            input untouched).
        scope (str): Variable scope name.

    Returns:
        Model: Updated network Model.
    """

    with tf.variable_scope(scope):
        layer = tf.layers.dropout(
            network.output,
            rate=rate,
            training=training
        )

    network.outputs.append(layer)
    return network


def fully_connected(network, num_outputs, scope="fc", sigmoid=True):
    """
    Add a fully connected layer to network model.

    Args:
        network (Model): A network Model instance to use.
        num_outputs (int): The number of output units in the layer.
        scope (str): Variable scope name.
        sigmoid (bool): Sigmoid activation function usage indicator. If False,
            use ReLU instead.

    Returns:
        Model: Updated network Model.
    """

    activation_fn = tf.sigmoid if sigmoid else tf.nn.relu

    with tf.variable_scope(scope):
        fully = tf.contrib.layers.fully_connected(
            network.output,
            num_outputs,
            activation_fn=activation_fn
        )

    network.outputs.append(fully)
    return network


def reshape(network, shape, scope='reshape'):
    """
    Reshape last network layer.

    Args:
        network (Model): A network Model instance to use.
        shape (Tensor): Must be one of the following types: int32, int64.
            Defines the shape of the output tensor.
        scope (str): Variable scope name.

    Returns:
        Model: Updated network Model.
    """

    with tf.variable_scope(scope):
        layer = tf.reshape(network.output, shape)

    network.outputs.append(layer)
    return network
