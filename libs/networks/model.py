"""
An abstract neural network model.
"""

import tensorflow as tf


class Model:
    """
    An abstract neural network model.

    Args:
        features (tensorflow.Placeholder): A placeholder for input values.
        name (str): A name used as a variable_scope when running.
    """
    def __init__(self, features, name):
        self.name = name
        self.outputs = [features]
        self.session = None

    @property
    def input_size(self):
        """
        Return output layer output size.

        Returns:
            int: Output layer output size.
        """
        return int(self.output.get_shape()[-1])

    @property
    def depth(self):
        """
        Return the number of layers in a network.

        Returns:
            int: The number of layers in this network.
        """
        return len(self.outputs)

    @property
    def input(self):
        """
        Return the input layer placeholder.

        Returns:
            tensorflow.Placeholder: Return network input layer placeholder.
        """
        return self.outputs[0]

    @property
    def output(self):
        """
        Return the network output layer.

        Returns:
            tensorflow.Tensor: Network output layer.
        """
        return self.outputs[-1]

    def run(self, batch, session=None):
        """
        Run network in a Session session for input batch.

        Args:
            batch (ndarray): Evaluation input.
            session (tensorflow.Session):

        Returns:
            tensorflow.Tensor: Evaluation result.
        """
        sess = self.session if session is None else session

        if sess is None:
            return None

        return sess.run(self.output, feed_dict={self.input: batch})

    def __call__(self, batch):
        """
        Model.run() wrapper as an object function call.

        Args:
            batch (ndarray): Evaluation input.

        Returns:
            tensorflow.Tensor: Evaluation result.
        """
        return self.run(batch)

    @property
    def variables(self):
        """
        Return all model variables.

        Returns:
            The list of values in the collection with the given name, or an
            empty list if no value has been added to that collection.
            The list contains the values in the order under which they were
            collected.
        """
        return tf.get_collection(
            tf.GraphKeys.GLOBAL_VARIABLES,
            scope=self.name
        )
