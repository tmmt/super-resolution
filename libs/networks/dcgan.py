"""
DCGAN network model.

Todo:
    * Use dropout, batch_normalization and residual layers.
    * Add x4 scaling support.
    * Add nonhardcoded support for grayscale images.
"""

import datetime
import os
import time

import numpy as np
import scipy.misc
from skimage.measure import compare_psnr, compare_ssim
import tensorflow as tf

from . import layers
from . import model


FLAGS = tf.app.flags.FLAGS


class Generator(model.Model):
    """
    Generator network.

    Args:
        width (int): Input image width
        height (int): Input image height
        colors (bool): Input image colors indicator
    """
    def __init__(self, width=16, height=16, colors=True):
        self.width = width
        self.height = height
        self.colors = 3 if colors else 1
        features = tf.placeholder(
            tf.float32, shape=[None, width, height, self.colors]
        )

        super().__init__(features, "discriminator")

        with tf.variable_scope(self.name):
            self = layers.conv2d_transpose_block(self, 256, 3, 'g_deconv1', tf.nn.relu)
            self = layers.conv2d_transpose_block(self, 128, 2, 'g_deconv2', tf.nn.relu)
            self = layers.conv2d_transpose_block(self, 64, 2, 'g_deconv3', tf.nn.relu)
            self = layers.conv2d_block(self, 32, 2, 'g_conv1', tf.nn.relu)
            self = layers.conv2d_block(self, self.colors, 2, 'g_conv2', tf.sigmoid)

    def predict(self, dataset, output, checkpoint, compare=False):
        """
        Run generator model on a given dataset.

        Args:
            dataset (Dataset): A Dataset of files used as input.
            output (Folder): A Folder used for saving results.
            checkpoint (str): A path to generator network weights checkpoint.
            compare (bool): PSNR and SIMM comparison indicator.
        """
        if compare:
            measures = {
                "generator": {
                    "psnr": np.zeros(dataset.size, dtype=np.float32),
                    "simm": np.zeros(dataset.size, dtype=np.float32)
                },
                "bicubic": {
                    "psnr": np.zeros(dataset.size, dtype=np.float32),
                    "simm": np.zeros(dataset.size, dtype=np.float32)
                }
            }

        folders = {
            "generated": output.makedirs('generated'),
            "16x16": output.makedirs('16x16'),
            "32x32": output.makedirs('32x32'),
            "cubic": output.makedirs('cubic')
        }

        with tf.Session() as sess:
            tf.train.Saver().restore(sess, checkpoint)
            self.session = sess

            index = 0
            for small, original in dataset(FLAGS.batch_size):
                generated = self(small)
                cubic = sess.run(tf.image.resize_bicubic(small, (32, 32)))

                for i, photo in enumerate(generated):
                    if compare:
                        measures['generator']['psnr'][index] = compare_psnr(
                            photo, original[i], data_range=1.0
                        )
                        measures['generator']['simm'][index] = compare_ssim(
                            photo, original[i],
                            multichannel=True, data_range=1.0
                        )
                        measures['bicubic']['psnr'][index] = compare_psnr(
                            cubic[i], original[i], data_range=1.0
                        )
                        measures['bicubic']['simm'][index] = compare_ssim(
                            cubic[i], original[i],
                            multichannel=True, data_range=1.0
                        )

                    scipy.misc.toimage(photo, cmin=0.0, cmax=1.0).save(
                        os.path.join(
                            folders['generated'],
                            os.path.basename(dataset.files[index])
                        )
                    )

                    scipy.misc.toimage(small[i], cmin=0.0, cmax=1.0).save(
                        os.path.join(
                            folders['16x16'],
                            os.path.basename(dataset.files[index])
                        )
                    )

                    scipy.misc.toimage(original[i], cmin=0.0, cmax=1.0).save(
                        os.path.join(
                            folders['32x32'],
                            os.path.basename(dataset.files[index])
                        )
                    )

                    scipy.misc.toimage(cubic[i], cmin=0.0, cmax=1.0).save(
                        os.path.join(
                            folders['cubic'],
                            os.path.basename(dataset.files[index])
                        )
                    )
                    index += 1

            if compare:
                print("dcgan")
                print("psnr =", np.mean(measures['generator']['psnr']))
                print("simm =", np.mean(measures['generator']['simm']))
                print()
                print("bicubic")
                print("psnr =", np.mean(measures['bicubic']['psnr']))
                print("simm =", np.mean(measures['bicubic']['simm']))

    def loss(self):
        """
        Generator loss function.

        Returns:
            A 5-tuple containing original tensor, generated tensor,
                disciminator fake output, loss function and an
                AdamOptimizer.minimize() instance.
        """
        global_step = tf.Variable(0, name='g_global_step', trainable=False)

        if FLAGS.original_loss:
            original = tf.placeholder(
                tf.float32,
                shape=[None, self.width * 2, self.height * 2, self.colors]
            )
            generated = self.output
        else:
            original = self.input
            generated = tf.placeholder(
                tf.float32,
                shape=[None, self.width, self.height, self.colors]
            )

        difference = tf.reduce_mean(
            tf.abs(original - generated),
            name='difference'
        )

        fake = tf.placeholder(tf.float32, shape=[None, 1])

        loss = tf.reduce_mean(
            tf.add(
                - (1 - FLAGS.generator_loss_factor) * tf.log(1 - fake),
                FLAGS.generator_loss_factor * difference,
                name='gen_loss'
            )
        )

        adam = tf.train.AdamOptimizer(learning_rate=FLAGS.learning_rate,
                                      beta1=FLAGS.adam_beta1,
                                      name='gen_adam')

        minimize = adam.minimize(
            loss,
            var_list=self.variables,
            name='gen_loss_minimize',
            global_step=global_step
        )

        return original, generated, fake, loss, minimize


class Discriminator(model.Model):
    """
    Discriminator network.

    Args:
        width (int): Input image width
        height (int): Input image height
        colors (bool): Input image colors indicator
    """
    def __init__(self, width=32, height=32, colors=True):
        colors = 3 if colors else 1
        features = tf.placeholder(
            tf.float32, shape=[None, width, height, colors]
        )

        super().__init__(features, "discriminator")

        with tf.variable_scope(self.name):
            self = layers.conv2d_block(self, 64, 3, 'd_conv1', tf.nn.relu)
            self = layers.conv2d_block(self, 128, 2, 'd_conv2', tf.nn.relu)
            self = layers.conv2d_block(self, 256, 2, 'd_conv3', tf.nn.relu)
            self = layers.conv2d_block(self, 512, 2, 'd_conv4', tf.nn.relu)
            self = layers.conv2d_block(self, 1024, 2, 'd_conv5', tf.nn.relu)
            self = layers.fully_connected(self, 512, 'd_fc1')
            self = layers.fully_connected(self, 1, 'd_fc2', sigmoid=True)
            self = layers.reshape(self, (-1, 1))

    def real(self):
        """
        Loss function for a real batch.

        Returns:
            A 3-tuple consisting of fake placeholder, loss function and an
                AdamOptimizer.minimize() instance.
        """
        global_step = tf.Variable(0, name='dr_global_step', trainable=False)

        fake = tf.placeholder(tf.float32, shape=[None, 1])

        loss = tf.add(
            tf.reduce_mean(-tf.log(self.output)),
            tf.reduce_mean(-tf.log(tf.ones_like(fake) - fake))
        )


        adam = tf.train.AdamOptimizer(learning_rate=FLAGS.learning_rate,
                                      beta1=FLAGS.adam_beta1,
                                      name='disc_adam_fake')


        minimize = adam.minimize(
            loss,
            var_list=self.variables,
            name='disc_loss_minimize_real',
            global_step=global_step
        )

        return fake, loss, minimize


    def fake(self):
        """
        Loss function for a fake batch.

        Returns:
            A 3-tuple consisting of real placeholder, loss function and an
                AdamOptimizer.minimize() instance.
        """
        global_step = tf.Variable(0, name='df_global_step', trainable=False)

        real = tf.placeholder(tf.float32, shape=[None, 1])

        loss = tf.add(
            tf.reduce_mean(-tf.log(real)),
            tf.reduce_mean(
                -tf.log(
                    tf.ones_like(self.output)
                    - self.output
                )
            )
        )

        adam = tf.train.AdamOptimizer(learning_rate=FLAGS.learning_rate,
                                      beta1=FLAGS.adam_beta1,
                                      name='disc_adam_fake')

        minimize = adam.minimize(
            loss,
            var_list=self.variables,
            name='disc_loss_minimize_fake',
            global_step=global_step
        )

        return real, loss, minimize


class DCGAN:
    """
    DCGAN network implementation.

    Args:
        width (int): Input image width
        height (int): Input image height
        colors (bool): Input image colors indicator
    """
    def __init__(self, width=16, height=16, colors=True):
        self.width = 16
        self.height = 16
        self.colors = 3 if colors else 1

        self.generator = Generator(width, height, colors)
        self.discriminator = Discriminator(width * 2, height * 2, colors)

    def train(self, dataset, validation, folder, epochs=10):
        """
        Train DCGAN model on a given dataset. Validate loss function values
        using a validation Dataset.

        Args:
            dataset (Dataset): A Dataset used for training.
            validation (Dataset): A Dataset used for validation.
            folder (Folder): A Folder used saving results.
            epochs (int): Number of epochs to run.

        Todo:
            * Add support for different batch sizes for generator and
                discriminator.
            * Use different batches for generator and discriminator loss
                functions optimization. Discriminator converges much faster so
                Generator should train much more.
            * Add training continuation
        """
        g_checkpoint = folder.makedirs('checkpoints/generator')
        d_checkpoint = folder.makedirs('checkpoints/discriminator')

        _g = g_original, g_generated, g_fake, g_loss, g_min = self.generator.loss()
        _dr = dr_fake, dr_loss, dr_min = self.discriminator.real()
        _df = df_real, df_loss, df_min = self.discriminator.fake()

        g_saver = tf.train.Saver(self.generator.variables)
        d_saver = tf.train.Saver(self.discriminator.variables)

        sess = tf.Session()

        # TODO: load from checkpoint if FLAGS.continue == True
        init_op = tf.global_variables_initializer()
        sess.run(init_op)

        self.generator.session = sess
        self.discriminator.session = sess

        step = 0
        example = 0
        start_time = time.time()

        for epoch in range(epochs):
            for small, original in dataset(FLAGS.batch_size):
                step += 1
                batch_size = len(small)
                example += batch_size

                generated = self.generator(small)
                fake = self.discriminator(generated)
                # real = self.discriminator(original[batch_size // 2:])
                real = self.discriminator(original)

                feed_dict = {
                    self.generator.input: small[:FLAGS.batch_size // 4],
                    g_fake: fake[:FLAGS.batch_size // 4]
                }

                if FLAGS.original_loss:
                    feed_dict[g_original] = original[:FLAGS.batch_size // 4]
                else:
                    feed_dict[g_generated] = sess.run(
                        tf.image.resize_bicubic(
                            generated[:FLAGS.batch_size // 4], (16, 16)
                        )
                    )

                _, _g_loss = sess.run(
                    [g_min, g_loss],
                    feed_dict=feed_dict
                )

                _, _dr_loss = sess.run(
                    [dr_min, dr_loss],
                    feed_dict={
                        self.discriminator.input: original,
                        dr_fake: fake
                    }
                )

                _, _df_loss = sess.run(
                    [df_min, df_loss],
                    feed_dict={
                        self.discriminator.input: generated,
                        df_real: real
                    }
                )

                if step % 10 == 0:
                    print("""
###############################################################################
step: %09d | epoch: %05d | examples: %013d

           Generator loss: \033[32m%lf\033[0m
[fake] Discriminator loss: \033[31m%lf\033[0m
[real] Discriminator loss: \033[33m%lf\033[0m              [time elapsed]: %-16s
###############################################################################
""" % (step, epoch, example, _g_loss, _df_loss, _dr_loss,
       str(datetime.timedelta(seconds=time.time() - start_time))))

                    with open(os.path.join(folder.session, 'training'), 'a') as f:
                        f.write(
                            '%d, %d, %d, %lf, %lf, %lf\n' % (
                                step, epoch, example,
                                _g_loss, _df_loss, _dr_loss
                            )
                        )

                    self._progress(dataset, folder, step)
                    _validation = np.zeros(
                        shape=(validation.batches(FLAGS.batch_size), 3)
                    )
                    for i, batch in enumerate(validation(FLAGS.batch_size)):
                        _validation[i] = self.losses(
                            batch,
                            _g, _dr, _df
                        )

                    v_g_loss, v_dr_loss, v_df_loss = np.average(
                        _validation, axis=0
                    )

                    with open(os.path.join(folder.session, 'validation'), 'a') as f:
                        f.write(
                            '%d, %d, %d, %lf, %lf, %lf\n' % (
                                step, epoch, example,
                                v_g_loss, v_dr_loss, v_df_loss
                            )
                        )

                if step % FLAGS.checkpoint_period == 0:
                    g_saver.save(
                        sess,
                        os.path.join(g_checkpoint, 'model'),
                        global_step=step
                    )
                    d_saver.save(
                        sess,
                        os.path.join(d_checkpoint, 'model'),
                        global_step=step
                    )

    def _progress(self, dataset, folder, step, batch_size=16):
        """
        A helper function used to generate images on a first few photos from a
        dataset.

        Args:
            dataset (Dataset): Input dataset.
            folder (Folder): Save folder.
            step (int): Step counter used for folder names.
            batch_size (int): Number of images to use.
        """
        folders = {
            '32x32': folder.makedirs('progress/%06d/32x32' % step),
            '16x16': folder.makedirs('progress/%06d/16x16' % step),
            'dcgan': folder.makedirs('progress/%06d/dcgan' % step),
            'bicubic': folder.makedirs('progress/%06d/cubic' % step)
        }

        small, original = dataset(batch_size).__next__()
        bicubic = self.generator.session.run(tf.image.resize_bicubic(
            small, (self.width * 2, self.height * 2)
        ))

        for i, photo in enumerate(self.generator(small)):
            scipy.misc.toimage(small[i], cmin=0.0, cmax=1.0).save(
                os.path.join(
                    folders['16x16'],
                    os.path.basename(dataset.files[i])
                )
            )

            scipy.misc.toimage(original[i], cmin=0.0, cmax=1.0).save(
                os.path.join(
                    folders['32x32'],
                    os.path.basename(dataset.files[i])
                )
            )

            scipy.misc.toimage(photo, cmin=0.0, cmax=1.0).save(
                os.path.join(
                    folders['dcgan'],
                    os.path.basename(dataset.files[i])
                )
            )

            scipy.misc.toimage(bicubic[i], cmin=0.0, cmax=1.0).save(
                os.path.join(
                    folders['bicubic'],
                    os.path.basename(dataset.files[i])
                )
            )

    def losses(self, batch, g_loss, dr_loss, df_loss):
        """
        Calculate loss functions for a given dataset.

        Args:
            batch (ndarray, ndarray): A single batch from a Dataset() call.
            g_loss (5-tuple): DCGAN.generator.loss() result.
            dr_loss (3-tuple): DCGAN.discriminator.real() result.
            df_loss (3-tuple): DCGAN.discriminator.fake() result.

        Returns:
            (float, float, float): Generator loss, Discriminator real loss and
                Discriminator fake loss.
        """
        small, original = batch
        batch_size = len(small)

        sess = self.generator.session

        generated = self.generator(small)
        fake = self.discriminator(generated)
        real = self.discriminator(original)

        g_original, g_generated, g_fake, g_loss, _ = g_loss
        dr_fake, dr_loss, _ = dr_loss
        df_real, df_loss, _ = df_loss

        feed_dict = {
            self.generator.input: small,
            g_fake: fake
        }

        if FLAGS.original_loss:
            feed_dict[g_original] = original
        else:
            feed_dict[g_generated] = sess.run(
                tf.image.resize_bicubic(generated, (16, 16))
            )

        _g_loss = sess.run(g_loss, feed_dict=feed_dict)

        _dr_loss = sess.run(
            dr_loss,
            feed_dict={
                self.discriminator.input: original,
                dr_fake: fake
            }
        )

        _df_loss = sess.run(
            df_loss,
            feed_dict={
                self.discriminator.input: generated,
                df_real: real
            }
        )

        return _g_loss, _dr_loss, _df_loss
