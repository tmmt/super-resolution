"""
A dataset module representing multiple ndarray instances with a batch generator.
"""

import datetime
import glob
import os
import time

import numpy as np
from PIL import Image


class Dataset:
    """
    Image array wrapper with a batch generator.

    Args:
        dataset (str): A

    Todo:
        * Add shuffle support
        * Use tensorflow for dataset image resize operations
    """
    def __init__(self, dataset, width=32, height=32, colors=True):
        self.width = width
        self.height = height
        self.colors = 3 if colors else 1
        self.normalization = True

        self.files = []
        for f in glob.glob(dataset):
            self.files.append(os.path.abspath(
                os.path.join(os.path.dirname(dataset), os.path.basename(f))
            ))

        if not self.files:
            for f in os.listdir(dataset):
                self.files.append(os.path.abspath(
                    os.path.join(dataset, os.path.basename(f))
                ))


        self.original = np.zeros(
            (self.size, width, height, self.colors), dtype=np.float32
        )
        self.small = np.zeros(
            (self.size, width // 2, height // 2, self.colors), dtype=np.float32
        )

        self._load()

    def _load(self):
        """
        Load dataset images into into ndarray. Create a bigger and a smaller
        (twice) copy.

        Todo:
            * Ignore the files that are not compatible images.
        """
        start_time = time.time()
        for i, path in enumerate(self.files):
            if i % 1000 == 0:
                print(
                    "% 8d:" % i,
                    str(datetime.timedelta(seconds=time.time() - start_time))
                )

            img = Image.open(path).convert('RGB')
            ratio = img.size[1] / img.size[0]

            b_img = img.resize((self.width, int(self.height * ratio)), Image.ANTIALIAS)
            s_img = img.resize((self.width // 2, int((self.height / 2) * ratio)), Image.ANTIALIAS)

            b_shift = int((self.height * ratio) - self.width) // 2
            s_shift = int(((self.height / 2) * ratio) - self.width / 2) // 2

            # crop larger photos
            b_pix = np.array(b_img)[b_shift:b_shift + self.width, :self.width]
            s_pix = np.array(s_img)[s_shift:s_shift + (self.width // 2), :(self.width // 2)]

            # normalization
            if self.normalization:
                b_pix = b_pix / 255
                s_pix = s_pix / 255

            self.original[i][:, :] = b_pix
            self.small[i][:, :] = s_pix

        print(
            "Finished:",
            str(datetime.timedelta(seconds=time.time() - start_time))
        )

    def __call__(self, batch_size, shuffle=False):
        """Dataset batch generator

        Generate multiple batches of images from this Dataset instance.

        Args:
            batch_size (int): A number of examples per batch.
            shuffle (bool): Dataset shuffle indicator. Not yet implemented.

        Yields:
            (ndarray, ndarray): An ordered pair of small and original images
                each containing at most batch_size images.

        Todo:
            * Shuffle dataset before each generator call.
        """
        for batch in range(self.batches(batch_size)):
            yield self.small[batch * batch_size:(batch + 1) * batch_size],\
                  self.original[batch * batch_size:(batch + 1) * batch_size]

    @property
    def size(self):
        """Dataset length.

        Number of images in a dataset instance.

        Returns:
            int: Dataset length.
        """
        return len(self.files)

    def batches(self, batch_size):
        """Number of batches.

        Number of batches for a given batch_size.

        Args:
            batch_size (int): Batch size to use.

        Returns:
            int: Number of batches.
        """
        return self.size // batch_size + (self.size % batch_size > 0)
